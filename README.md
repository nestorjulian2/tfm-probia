## Instalación y Entorno Virtual


En la carpeta del repositorio, crear carpeta entornos = **mkdir entornos**

instalar primero **Anaconda** y ejecutar CDM.exe Prompt y desde allí ir a la carpeta de entornos del proyecto y crearlo así: **conda create -n tfm-MaskRCNN anaconda python=3.7.7** y luego **conda install -r requirements/local.txt**

instrucciones para instalar entornoVirtual con GPU https://www.youtube.com/watch?v=FZIuiIE28NY 

**Crear entorno virtual**

En la carpeta entornos, **python -m venv ent-viton** posteriormente se debe activar el entorno, entrando a la carpeta **ent-viton/scripts** y se ejecuta el comando **.\activate** para activar el entorno
en caso de que exista restricción en la ejecución de scripts en windows, ejecutar powershell como administrador y luego **Set-ExecutionPolicy Unrestricted**

Ahora se ubica en la carpeta del repositorio y se ejecuta, **pip install -r requirements/local.txt**, y luego se verifica **pip freeze --local**


## Entorno GPU 

**Preparación del entorno**
-
	$ conda create -n entornoGPU anaconda python=3.7.7
	$ conda activate entornoGPU
	$ conda install ipykernel
	$ python -m ipykernel install --user --name entornoGPU --display-name "entornoGPU"
	$ conda install tensorflow-gpu==2.1.0 cudatoolkit=10.1
	$ pip install tensorflow==2.1.0
	$ pip install jupyter
	$ pip install keras==2.3.1
	$ pip install scikit-image==0.16.2
	$ pip install numpy scipy Pillow cython matplotlib opencv-python h5py imgaug IPython[all]
	
	$ pip install -U scikit-image==0.16.2 **#en caso de tener instalado una versión superior**

**Probar versión y gpu**
	$ python
	$ import tensorflow as tf
	$ tf.__version__
	$ tf.test.gpu_device_name()

## Configurar consola VS con conda

**Fácil y rápido**
Cambie VS Code para usar cmd.exe como el terminal integrado predeterminado Shell:

abriendo la paleta de comandos (Control-Shift-P)
buscar Terminal: Select Default Shell
seleccione Command Prompt

**Más duro/Powershell**
agregue la ubicación de conda a su RUTA (si no la agregó a través del instalador). Para mí en una instalación de "Todos los usuarios" esto es C:\ProgramData\Anaconda\Scripts
desde un símbolo de administrador Powershell, cambie la política de ejecución de Powershell a firma remota, es decir, Set-ExecutionPolicy RemoteSigned
abra un mensaje de Anaconda y ejecute conda init powershell que agregará el inicio relacionado con Conda a un Powershell profile.ps1 en algún lugar del perfil de su usuario.

## Instalar MaskRCNN
	$ python setup.py install
	$ pip install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI

## Ejecutar proyecto

Para ejecutar el proyecto, se debe tener activado el entorno **ent-viton** y dirigir a la carpeta **/probia** dentro del repositorio y ejecutar **python manage.py runserver**