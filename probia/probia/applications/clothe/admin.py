from django.contrib import admin
from .models import Clothe,Tipo,Estilo,Marca,Categoria,Talla,Foto,DisponibilidadTalla
# Register your models here.
from .models import (
    Tipo, 
    Estilo,
    Marca, 
    Categoria, 
    Clothe, 
    Talla, 
    Foto, 
    DisponibilidadTalla
)

# Register your models here.

admin.site.register(Tipo)
admin.site.register(Estilo)
admin.site.register(Marca)
admin.site.register(Categoria)
admin.site.register(Talla)
admin.site.register(Foto)
admin.site.register(DisponibilidadTalla)

class PrendaAdmin(admin.ModelAdmin):
    list_display =(
        'id','fotoPromocional','referencia', 'nombre', 'tienda', 'tipo', 'marca'
    )
    search_fields =('nombre',)
    list_filter=('tipo','tienda','marca','estilo')
    filter_horizontal =('categorias',)

admin.site.register(Clothe, PrendaAdmin)