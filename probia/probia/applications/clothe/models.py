from django.db import models

from probia.applications.shop.models import Shop
from ckeditor.fields import RichTextField

# Create your models here.
#se debe crear un listado de fotos, pero definir una foto para predicciones
class Tipo(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self): return self.nombre

class Estilo(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self): return self.nombre

class Marca(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self): return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self): return self.nombre

class Clothe(models.Model):
    referencia = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    descripcion = RichTextField()
    precioFinal = models.IntegerField()
    tienda= models.ForeignKey(Shop, on_delete=models.CASCADE)
    tipo=models.ForeignKey(Tipo, on_delete=models.CASCADE)
    estilo=models.ForeignKey(Estilo, on_delete=models.CASCADE)
    fotoPromocional = models.ImageField(upload_to='prendas', height_field=None, width_field=None, max_length=None)
    fotoProbar = models.ImageField('Foto a Probar', upload_to='prendas', height_field=None, width_field=None, max_length=None)
    categorias = models.ManyToManyField(Categoria)
    marca= models.ForeignKey(Marca, on_delete=models.CASCADE)

    def __str__(self): return self.nombre+' - '+str(self.id)


class Talla(models.Model):
    talla = models.CharField(max_length=10)
    tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    estilo= models.ForeignKey(Estilo, on_delete=models.CASCADE)
    marca= models.ForeignKey(Marca, on_delete=models.CASCADE)
    contornoPechoMin=models.DecimalField('Contorno Pecho Mìnimo', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoPechoMax=models.DecimalField('Contorno Pecho Màximo', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoCinturaMin=models.DecimalField('Contorno Cintura', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoCinturaMax=models.DecimalField('Contorno Cintura', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoCaderaMin=models.DecimalField('Contorno Cadera', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoCaderaMax=models.DecimalField('Contorno Cadera', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoBrazoMin=models.DecimalField('Contorno Brazo', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoBrazoMax=models.DecimalField('Contorno Brazo', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoPiernaMin=models.DecimalField('Contorno Pierna', max_digits=5, decimal_places=2, editable=True, default=0.0)
    contornoPiernaMax=models.DecimalField('Contorno Pierna', max_digits=5, decimal_places=2, editable=True, default=0.0)


class Foto(models.Model):
    prenda = models.ForeignKey(Clothe, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='prendas', height_field=None, width_field=None, max_length=None)


class DisponibilidadTalla(models.Model):
    prenda = models.ForeignKey(Clothe, on_delete=models.CASCADE, null=True)
    cantidad = models.IntegerField()
    talla= models.ForeignKey(Talla, on_delete=models.CASCADE)


