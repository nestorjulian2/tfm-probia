from django.shortcuts import render
from django.views.generic import TemplateView, ListView


# Create your views here.
class HomePage(TemplateView):
    template_name='home/index.html'
    