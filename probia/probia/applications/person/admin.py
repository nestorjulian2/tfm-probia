from django.contrib import admin

# Register your models here.
from .models import (
    Person,
    PrendasPersona
)

# Register your models here.


class PersonaAdmin(admin.ModelAdmin):
    list_display =(
        'nombre_persona', 'email', 'fotoFrontal'
    )
    search_fields =('nombre',)
    list_filter=('genero',)

    def nombre_persona(self, obj):
        return obj.nombres+' '+obj.apellidos

admin.site.register(Person, PersonaAdmin)
admin.site.register(PrendasPersona)