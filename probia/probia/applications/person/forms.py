from django import forms 
from .models import Person
from django.contrib.auth import authenticate

class LoginForm(forms.Form):
    email = forms.CharField(
        label='Email',
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder':'Ingrese su email',
                'style':'{margin: 10px}',
            }
        )
    )

    password=forms.CharField(
        label='Contraseña',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña',
                'style':'{margin: 10px}',
            }
        )
    )
    def clean(self):
        cleaned_data = super(LoginForm,self).clean()
        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        if not authenticate(email=email, password=password):
            raise forms.ValidationError('Los datos del ususario no son correctos')

        return self.cleaned_data


class PersonRegisterForm(forms.ModelForm):
    """Form definition for PersonRegister."""

    password1=forms.CharField(
        label='Contraseña',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña'
            }
        )
    )

    password2=forms.CharField(
        label='Contraseña',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Repetir contraseña'
            }
        )
    )

    class Meta:
        """Meta definition for PersonRegisterform."""

        model = Person
        fields = (
            'nombres',
            'apellidos',
            'email',
            'celular',
            'genero',
            'fotoFrontal',
            'fotoLateral',
            'estatura',
            'edad',
            'fechaNacimiento',
        )
    
    def clean_password2(self): 
        if self.cleaned_data['password1']!=self.cleaned_data['password2']:
            self.add_error('password2','Las contraseñas no coinciden')

class UpdatePasswordForm(forms.Form):
    password1=forms.CharField(
        label='Contraseña Actual',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña Actual'
            }
        )
    )
    password2=forms.CharField(
        label='Contraseña Nueva',
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña Nueva'
            }
        )
    )

    '''
    def clean_password1(self):
        cleaned_data = super(UpdatePasswordForm,self).clean()
        email = self.user.email
        password = self.cleaned_data['password1']
        if not authenticate(email=email, password=password):
            raise forms.ValidationError('la contraseña actual ingresada es incorrecta')
        return self.cleaned_data 
    
    '''

    def clean_password2(self): 
        if self.cleaned_data['password1']==self.cleaned_data['password2']:
            self.add_error('password2','La contraseña nueva es igual a la actual')