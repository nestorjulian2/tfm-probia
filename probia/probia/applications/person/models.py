from django.db import models
from probia.applications.clothe.models import Clothe
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import UserManager

# Create your models here.
class Person(AbstractBaseUser, PermissionsMixin):

    GENEROS=(
        ('0','Mujer'),
        ('1','Hombre'),
        ('2','Otro'),
        ('3','No definido'),
    )

    nombres = models.CharField(max_length=100, blank=True)
    apellidos = models.CharField(max_length=100, blank=True)
    email = models.EmailField(null=False, unique=True)
    celular= models.CharField(max_length=20, blank=True)
    genero = models.CharField(max_length=1, choices=GENEROS, default='3')
    fotoFrontal = models.ImageField('Foto Frontal',upload_to='photos', null=True, blank=True)
    fotoFrontalTratada= models.ImageField(upload_to='photos', null=True, editable=False, blank=True)
    fotoLateral = models.ImageField('Foto Lateral',upload_to='photos', null=True, blank=True)
    fotoLateralTratada = models.ImageField(upload_to='photos', null=True, editable=False, blank=True)
    contornoPecho=models.DecimalField('Contorno Pecho', max_digits=5, decimal_places=2, editable=False, default=0.0)
    contornoCintura=models.DecimalField('Contorno Cintura', max_digits=5, decimal_places=2, editable=False, default=0.0)
    contornoCadera=models.DecimalField('Contorno Cadera', max_digits=5, decimal_places=2, editable=False, default=0.0)
    contornoBrazo=models.DecimalField('Contorno Brazo', max_digits=5, decimal_places=2, editable=False, default=0.0)
    contornoPierna=models.DecimalField('Contorno Pierna', max_digits=5, decimal_places=2, editable=False, default=0.0)
    estatura=models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    edad=models.IntegerField(blank=True,null=True)
    fechaNacimiento=models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    fechaRegistro=models.DateField(auto_now=False, auto_now_add=True)
    fechaActualizacion=models.DateField(auto_now=True, auto_now_add=False)

    is_staff=models.BooleanField(default=False)

    USERNAME_FIELD='email'

    objects=UserManager()
    
    def get_short_name(self):
        return self.nombres

    def get_full_name(self):
        return self.nombres + " "+self.apellidos

class PrendasPersona(models.Model):
    prenda = models.ForeignKey(Clothe, on_delete=models.CASCADE)
    persona = models.ForeignKey(Person, on_delete=models.CASCADE)
    prediccion = models.ImageField('Visualización', upload_to='predicciones', height_field=None, width_field=None, max_length=None)