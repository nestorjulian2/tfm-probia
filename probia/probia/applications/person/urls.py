from django.contrib import admin
from django.urls import path, reverse_lazy

from . import views

app_name='person_app'

urlpatterns = [
    path('person/view', views.PersonView.as_view()),
    path(
        'person/register', 
        views.PersonRegisterView.as_view(),
        name='person-register'
    ),
    
    path(
        'panel/', 
        views.Panel.as_view(),
        name='panel'
    ),

    path(
        'login/', 
        views.Login.as_view(),
        name='person-login'
    ),
    path(
        'logout/', 
        views.Logout.as_view(),
        name='person-logout'
    ),
    path(
        'updatepass/', 
        views.UpdatePassword.as_view(),
        name='person-updatepass'
    ),
]