from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect

from django.views.generic import (
    TemplateView, 
    CreateView,
    FormView,
    View,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Person

from .forms import (
    PersonRegisterForm, 
    LoginForm,
    UpdatePasswordForm,
)

# Create your views here.
class PersonView(TemplateView):
    template_name='person/view.html'
    model = Person
    context_object_name = 'person'

class PersonRegisterView(CreateView):
    template_name = "person/register.html"
    model = Person
    form_class = PersonRegisterForm
    #fields =['dni','nombres','apellidos','email','celular','genero','fotoFrontal','fotoLateral','estatura','edad','fechaNacimiento','password']
    #Aqui se debe indicar el home autenticado
    success_url = reverse_lazy('home_app:panel')

    def form_valid(self, form):

        Person.objects.create_user(
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            #is_staff=True,
            nombres=form.cleaned_data['nombres'],
            apellidos=form.cleaned_data['apellidos'],
            celular=form.cleaned_data['celular'],
            genero=form.cleaned_data['genero'],
            fotoFrontal=form.cleaned_data['fotoFrontal'],
            fotoLateral=form.cleaned_data['fotoLateral'],
            estatura=form.cleaned_data['estatura'],
            edad=form.cleaned_data['edad'],
            fechaNacimiento=form.cleaned_data['fechaNacimiento'],
            
        )

        return super(PersonRegisterView, self).form_valid(form)

class Login(FormView):
    template_name='person/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('person_app:panel')

    def form_valid(self, form): 
        user=authenticate(
            email=form.cleaned_data['email'],
            password=form.cleaned_data['password'],
        )
        login(self.request, user)
        return super(Login, self).form_valid(form)

class Panel(LoginRequiredMixin,TemplateView):
    template_name = "person/panel.html"
    login_url=reverse_lazy('person_app:person-login')

class Logout(View):

    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(
            reverse(
                'home_app:home'
            )
        )
class UpdatePassword(LoginRequiredMixin,FormView):
    template_name='person/updatePass.html'
    form_class = UpdatePasswordForm
    success_url = reverse_lazy('person_app:person-login')
    login_url=reverse_lazy('person_app:person-login')

    def form_valid(self, form): 
        usuario=self.request.user
        
        user=authenticate(
            email=usuario.email,
            password=form.cleaned_data['password1']
        )

        if user:
            newPassword=form.cleaned_data['password2']
            usuario.set_password(newPassword)
            usuario.save()

        logout(self.request)
        return super(UpdatePassword, self).form_valid(form)
    