from django.contrib import admin
from .models import Shop
# Register your models here.

class TiendaAdmin(admin.ModelAdmin):
    list_display=(
        'nombre','paginaWeb','logo'
    )
    search_fields =('nombre',)

admin.site.register(Shop, TiendaAdmin)