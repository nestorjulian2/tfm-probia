# Generated by Django 3.1.7 on 2021-04-01 05:18

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo', models.ImageField(null=True, upload_to='tiendas')),
                ('nombre', models.CharField(max_length=50, verbose_name='Nombre')),
                ('descripcion', ckeditor.fields.RichTextField()),
                ('direccion', models.CharField(max_length=100, null=True, verbose_name='Dirección')),
                ('email', models.EmailField(max_length=100, verbose_name='Email')),
                ('paginaWeb', models.URLField(max_length=50, null=True, verbose_name='Pagina Web')),
                ('facebook', models.URLField(max_length=50, null=True, verbose_name='Facebook')),
                ('instagram', models.URLField(max_length=50, null=True, verbose_name='Instagram')),
                ('twitter', models.URLField(max_length=50, null=True, verbose_name='Twitter')),
                ('youtube', models.URLField(max_length=50, null=True, verbose_name='YouTube')),
                ('celular', models.CharField(max_length=20, null=True, verbose_name='Celular')),
                ('latitud', models.FloatField(editable=False, null=True)),
                ('longitud', models.FloatField(editable=False, null=True)),
            ],
        ),
    ]
