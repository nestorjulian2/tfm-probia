from django.db import models

# Create your models here.
from ckeditor.fields import RichTextField

# Create your models here.
class Shop(models.Model):
    logo=models.ImageField(upload_to='tiendas', null=True)
    nombre=models.CharField('Nombre', max_length=50)
    descripcion = RichTextField()
    direccion = models.CharField('Dirección', max_length=100, null=True)
    email = models.EmailField('Email', max_length=100)
    paginaWeb=models.URLField('Pagina Web', max_length=50, null=True)
    facebook=models.URLField('Facebook', max_length=50, null=True)
    instagram=models.URLField('Instagram', max_length=50, null=True)
    twitter=models.URLField('Twitter', max_length=50, null=True)
    youtube=models.URLField('YouTube', max_length=50, null=True)

    celular = models.CharField('Celular', max_length=20, null=True)
    latitud = models.FloatField(editable=False, null=True)
    longitud = models.FloatField(editable=False, null=True)

    def __str__(self):
        return self.nombre+' '+self.paginaWeb

